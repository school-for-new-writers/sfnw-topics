import codecs
import json
import os
import re
import time
from typing import Dict, List

import frontmatter
import markdown2
from frontmatter import Post
from jinja2 import Template
import jinja2

from util import wl, wl_flush

lectures_path: str = 'lectures/'
templates: Dict[str, str] = {}


# Custom Jinja filters
def regex_replace(s, find, replace):
    return re.sub(find, replace, s)


jinja2.filters.FILTERS['regex_replace'] = regex_replace


# Load lecture files
files: List[Dict[str, any]] = []

fileList = os.listdir(lectures_path)

for file in fileList:

    if '.md' not in file:
        continue

    fm: Post = frontmatter.load(os.path.join(lectures_path, file))

    if len(fm.keys()) <= 0:
        continue

    data: Dict[str, any] = {
        'File': file.replace('.md', '.html').lower(),
        'Title': fm['Title'],
        'Author': fm['Author'],
        'Date': fm['Date'],
        'Category': fm['Category'],
        'Tags': fm['Tags'],
        'Body': fm.content,
    }
    files.append(data)


# Load templates
wl('Loading templates...')

templates['index'] = codecs.open('templates/index.jj', 'r', encoding='utf-8-sig').read()
index_template = Template(templates['index'])

templates['lecture'] = codecs.open('templates/lecture.jj', 'r', encoding='utf-8-sig').read()
lecture_template = Template(templates['lecture'])

templates['rss'] = codecs.open('templates/rss.jj', 'r', encoding='utf-8-sig').read()
rss_template = Template(templates['rss'])

wl('All loaded.')

wl('Generating cache busting...')

buster: Dict[str, str] = {}
static_files = os.listdir('public/static/')
for sf in static_files:
    key = re.sub('_+', '_', re.sub('[^a-zA-Z0-9]+', '_', sf.lower()))
    val = str(time.time()).replace('.', '')
    buster[key] = val
    wl(f' - {key} : {val}')

wl('Cache busting generated')

# Generate home page
wl('Generating home page...')

os.makedirs('public', exist_ok=True)
index_html = index_template.render(lectures=files, year=time.strftime('%Y'), buster=buster)
index_file = codecs.open(os.path.join('public', 'index.html'), 'w', encoding='utf-8-sig').write(index_html)

wl('Generated home page.')


# Generate lectures
wl('Generating lecture files...')

for f in files:
    f['Body'] = markdown2.markdown(f['Body'])
    lecture_html = lecture_template.render(lecture=f, year=time.strftime('%Y'), buster=buster)
    lecture_file = codecs.open(os.path.join('public', f['File']), 'w', encoding='utf-8-sig').write(lecture_html)

wl(f'Generated lecture pages.')


# Generate API endpoint
json_arr = []
for f in files:
    json_arr.append({
        'title': f['Title'],
        'author': f['Author'],
        'date': f['Date'],
        'tags': f['Tags'],
        'category': f['Category'],
        'url': f['File']
    })

with codecs.open(os.path.join('public', 'api.json'), 'w', encoding='utf-8') as fh:
    json.dump(json_arr, fh, ensure_ascii=False, indent=4, sort_keys=True)

wl('Generated API endpoint.')


# Generate RSS feed
rss_files = []
for f in files:
    nf = f
    nf['Date'] = (f['Date'] + ' ' + time.strftime('%z')).replace('  ', ' ')
    rss_files.append(f)

rss_xml = rss_template.render(lectures=rss_files, date=time.strftime("%a, %d %b %Y %H:%M:%S %z"))
rss_file = codecs.open(os.path.join('public', 'rss.xml'), 'w', encoding='utf-8-sig').write(rss_xml)

wl('Generated RSS file.')


# # Move static assets
# staticFiles = os.listdir('static/dist')
# os.makedirs('public/static', exist_ok=True)
# for sf in staticFiles:
#     out_dir = shutil.copyfile(f'static/dist/{sf}', f'public/static/{sf}')
#     wl(f' - Moved static/dist/{sf} to {out_dir}')
#
# wl('Moved all static files')

# Flush linewriter
wl_flush()
