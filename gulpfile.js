'use strict';

const gulp = require('gulp');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
sass.compiler = require('sass');
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");
const Fiber = require('fibers');

// CSS processors
const autoprefixer = require('autoprefixer');
const discard = require('postcss-discard-comments');
const nano = require('cssnano');

// JS processors
const ts = require('gulp-typescript');
const tsProject = ts.createProject('./tsconfig.json');
const terser = require('gulp-terser-js');

// Dirs
const src  = './static/src'
const dist = './public/static'


// CSS tasks
gulp.task('css', () => {
    const processors = [
        autoprefixer,
        discard({ removeAll: true }),
        nano({ preset: 'default' })
    ];

    return gulp.src(`${src}/*.sass`)
        .pipe(sourcemaps.init())               // Init maps
        .pipe(sass({fiber: Fiber}))    // Compile SASS
        .pipe(gulp.dest(dist))                 // Output the raw CSS
        .pipe(postcss(processors))             // Postprocess it
        .pipe(sourcemaps.write('./'))          // Write maps
        .pipe(rename({ suffix: '.min' })) // Add .min suffix
        .pipe(gulp.dest(dist))                 // Output minified CSS
});

gulp.task('watch:css', () => gulp.watch(`${src}/**/*.sass`, gulp.series('css')));

// JS tasks
gulp.task('js', () => {
    return gulp.src([`${src}/**/*.js`])
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.init())
        .pipe(terser({
            mangle: {
                toplevel: true,
                keep_classnames: true,
                keep_fnames: true
            },
            keep_classnames: true,
            keep_fnames: true
        }))
        .on('error', err => {
            console.error(err)
            this.emit('end')
        })
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dist));
});

gulp.task('watch:js', () => gulp.watch([`${src}/**/*.js`], gulp.series('js')));

// TS tasks
gulp.task('ts', () => {
    return gulp.src([`${src}/**/*.ts`])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(gulp.dest(dist))
        .pipe(rename({ suffix: '.min' }))
        .pipe(terser({
            mangle: {
                toplevel: true,
                keep_classnames: true,
                keep_fnames: true
            },
            keep_classnames: true,
            keep_fnames: true
        }))
        .on('error', err => {
            console.error(err)
            this.emit('end')
        })
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dist));
});

gulp.task('watch:ts', () => gulp.watch([`${src}/**/*.ts`], gulp.series('ts')))

// All tasks
gulp.task('all', gulp.parallel(['css', 'js', 'ts']));
gulp.task('watch:all', gulp.parallel(['watch:css', 'watch:js', 'watch:ts', 'all']));
