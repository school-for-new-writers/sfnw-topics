(function (){
    let theme = window.localStorage.getItem('theme') ?? 'light'
    let body: Element = document.querySelector('body');
    let btn: HTMLButtonElement = <HTMLButtonElement>document.getElementById('theme-switch');

    let swapTheme = (theme: string) => {
        if (theme === 'dark') {
            body.classList.remove('light');
            body.classList.add('dark');
            btn.innerHTML = '☀';
            window.localStorage.setItem('theme', 'dark')
        } else {
            body.classList.remove('dark');
            body.classList.add('light');
            btn.innerHTML = '🌙'
            window.localStorage.setItem('theme', 'light')
        }
    }

    swapTheme(theme)

    btn.addEventListener('click', _ => swapTheme(window.localStorage.getItem('theme') === 'light' ? 'dark' : 'light'));
})()