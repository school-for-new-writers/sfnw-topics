function search() {

  let input = <HTMLInputElement>document.getElementById('search');
  let filter: string = input.value.toUpperCase();
  let lecture_box: HTMLElement = document.getElementById("lectures");
  let lectures: NodeListOf<Element> = lecture_box.querySelectorAll('a.lecture-card');

  lectures.forEach((l: Element) => {
    let name: string = (<HTMLElement>l).dataset.title;

    if (name.toUpperCase().indexOf(filter) > -1) {
      l.classList.remove('disabled')
    } else {
      l.classList.add('disabled')
    }
  })

}