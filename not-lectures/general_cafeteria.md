---
Author: Goldenwing
Category: ''
Date: Tuesday 20th of January 2015 @5:15am
Tags: []
Title: 'General: Cafeteria'
---

<p>Bored of study and academia? Looking for a place to speak nonsense or make subtle references to obscure pop culture? Come to the Cafeteria, and talk about whatever you want with your fellow students! There are no rules on what you can talk about here, just keep it clean and friendly!</p>