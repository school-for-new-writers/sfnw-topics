---
Author: The Dean
Category: ''
Date: Monday 2nd of March 2015 @7:38am
Tags: []
Title: 'Meta Activity: The Biggest FIMFiction Contest of the Year/Help Finding Editors'
---

<p>Good day students,</p><p>I have a very special extra curricular activity for you today. The School for New Writers is participating in a regional contest, The <a href="/blog/449458/the-efnw-2015-precon-contest" rel="nofollow">Everfree Northwest Pre-Con Contest</a>. This means writing a 1,000 - 3,000 word pg story related to the theme of making new friends. And be sure to read the details to make sure you aren't disqualified for submitting the wrong thing.</p><p>We are helping them judge and promote their contest in return for the same once we get to our own contests going. Only they have a lot more prizes, like, crazy awesome prizes. But, this will be a good experience for some of our staff as the great teacher migration of 2013 forced us to find <s>new sacrifices, </s>I mean Union workers who may not have had experience with contests before. </p><p>Of course, I expect all the winners to be from this school. We're disciplined here darn-it, and I demand nothing short of perfection. To be fair though, not everyone is perfect nor can everypony be a winner. As a school it is our duty to provide the resources you need to be the best you can be. So if you are participating in the contest, or want to help others who are, feel free to post bellow to help find yourselves editors and pre-readers for this event or to volunteer <s>your souls</s> time.</p><p>-To the victor go the spoils.</p>