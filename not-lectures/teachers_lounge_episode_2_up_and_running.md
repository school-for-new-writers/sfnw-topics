---
Author: darklordcomp
Category: ''
Date: Sunday 27th of September 2015 @4:42am
Tags: []
Title: Teachers Lounge Episode 2 up and running.
---

<p>Hey guy's sorry that it took so long to post this, Piquo wanted it to be perfect. So Join myself, Piquo and Piercing Sight along with guest speakers Pen Stroke and Gary Oak in the second episode of <a href="https://youtu.be/JfJOFe20Odc" rel="nofollow">Teachers Lounge!!</a> For those of you who haven't seen it yet here is the <a href="https://youtu.be/fgqBH8ijAw4" rel="nofollow">First Episode of the series.</a> The Teachers Lounge is a podcast of sorts where we the Professors (and guests writers) try and answer any questions you the students have that generally can't be explained through text. Enjoy!</p>