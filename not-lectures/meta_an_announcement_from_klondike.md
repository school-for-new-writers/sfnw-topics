---
Author: PegasusKlondike
Category: ''
Date: Thursday 11th of April 2013 @10:03pm
Tags: []
Title: 'Meta: An Announcement from Klondike'
---

<p>Greetings students and faculty! </p><p>Today I was treated to a rather special message. Wanderer D asked me to speak with M1sf0rtune, who wants me to be a part of his Equestrian Critics Society podcast this Saturday! The theme is author support/assistance groups (which is really saying something, that Wanderer D suggested us!), and I am really excited to be a part of his panel. </p><p><i>But</i>.... as horrid fate would have it, my brother has decided to start moving on Saturday and wants me to help, and I have no idea how long that will take. So, if it comes down to it, and I know for a fact that I won't be able to escape in time, I'm probably going to have one of you take over for me and represent the SFNW.</p>