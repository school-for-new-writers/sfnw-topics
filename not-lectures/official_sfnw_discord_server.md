---
Author: Angius
Category: ''
Date: Tuesday 26th of July 2016 @10:46pm
Tags: []
Title: Official SFNW Discord Server
---

<div class="bbcode-center" style="text-align:center"><p><img data-source="http://oi68.tinypic.com/2q18fbk.jpg" class="user_image" src="https://camo.fimfiction.net/OfVoLBMFAwXUQCVk-34gQ_bW4xGk2gFZ-gTJwORg1us?url=http%3A%2F%2Foi68.tinypic.com%2F2q18fbk.jpg" data-lightbox/=""></p></div><div class="bbcode-center" style="text-align:center"><p><span style="font-weight:bold;font-size:2em">WE HAVE A DISCORD SERVER NOW</span></p></div><hr><p>You heard that right! Our own Discord server for all of our group's needs! Ask! Discuss! Post pictures of ponies when mods are asleep!</p><p>Discord is free, doesn't require any download (though it exists and helps), doesn't even require an account! Offers unlimited number of users on an unlimited number of both text and voice channels! You heard that right! Want to talk with people live? We got you! Want a private channel for you and your proofreader so nobody has to download Skype or Teamspeak? We got you!</p><p>All you need to do is click that bigass link below and join us!</p><hr><div class="bbcode-center" style="text-align:center"><p><a href="https://discord.gg/MhuYbYU" rel="nofollow"><span style="font-weight:bold;font-size:2em">BIGASS LINK</span></a></p></div>