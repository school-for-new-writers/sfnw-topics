---
Author: Goldenwing
Category: ''
Date: Tuesday 4th of August 2015 @2:12am
Tags: []
Title: 'Teacher''s Lounge E1: First Original Edition!'
---

<p>Hello, lads and lasses! Good news to be had by all here, the first edition of The Teacher's Lounge has been finished, uploaded, and made all fancy.</p><p>Go check out <a href="https://youtu.be/fgqBH8ijAw4" rel="nofollow">The Teacher's Lounge</a>, where your favorite published pony professors discuss the finer points of horse words with the all the relaxed atmosphere of a blind date! We'll be tackling questions posed by you yourselves over the past while, providing all the elaborately detailed ponytalk that you so crave!</p><p>Tuning in on today's lecture is Piquo Pie, the headmaster who's name nobody is sure how to pronounce, Darklordcomp, the one that doesn't let silly things like "family friendly" hold him back, Mourning Zephyr, the guy with a cool name and real life responsibilities, and Piercing Sight, who has an absolutely adorable profile picture! I would've joined them too, as I know how much you lot love me, but unfortunately was unable to due to internet issues. Ah, well, I'll be in there next time!</p><p>The Teacher's Lounge will become a regular installment here at the SFNW, being done every month or whenever we get the energy to do it, whichever comes last. Got any suggestions for what questions we should discuss in the next episode? Ask away in the video comments or this very thread and, assuming we can make some interesting discussion over it, we'll do so!</p><p>Now run along, you rascals, I'm a busy man!</p>