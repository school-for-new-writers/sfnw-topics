---
Author: hailspider
Category: ''
Date: Tuesday 27th of January 2015 @11:14pm
Tags: []
Title: 'Meta/Guest: The School For New Reviewers'
---

<p><span style="font-size:0.5em">I have gotten permission to post this.</span><br>We're an <a href="/group/206180/school-for-new-reviewers/threads" rel="nofollow">unaffiliated group</a> dedicated to the training of reviewers through a combination of lectures (we need more lecturers!) and hands-on experiences.<br>If either teaching others how to improve their reviewing or learning how to improve your reviewing sound interesting, then this might be the group for you.</p>