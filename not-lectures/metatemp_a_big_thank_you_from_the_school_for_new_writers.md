---
Author: The Dean
Category: ''
Date: Saturday 24th of January 2015 @1:56pm
Tags: []
Title: 'Meta/Temp: A big THANK YOU from the School for New Writers'
---

<p>Good day Students,</p><p>I wanted to thank you all for being so wonderful. You see, The School for New Writers recently became <a href="/manage_user/groups?group_name=" rel="nofollow">the tenth biggest group on FIMfiction</a>.</p><p>I'm touched by the support you have shown this school and wanted to know that we will work all that much harder to live up to your expectations.</p><p>Now If you'll excuse me...<br><img data-source="https://i.imgur.com/ktu819Z.jpg" class="user_image" src="https://camo.fimfiction.net/yGkcjY-E-eja-ZFf_9VosYKlKodIcI5JM2CNmaWv1lk?url=https%3A%2F%2Fi.imgur.com%2Fktu819Z.jpg" data-lightbox/=""></p>