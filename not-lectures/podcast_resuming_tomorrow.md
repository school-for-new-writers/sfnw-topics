---
Author: darklordcomp
Category: ''
Date: Thursday 29th of September 2016 @4:41am
Tags: []
Title: Podcast Resuming Tomorrow.
---

<p>Hello my students! </p><p>I apologize for not posting for a long time, real life decided to throw itself at me and I couldn't really tell her no. Now that she has been satisfied I'm going to be starting the School For New Writers Podcast up once again, Tomorrow at 5:30pm PDT is when we will begin recording so post your questions! I'm hoping for a shorter cleaner Podcast so please come down here and contribute! The first episode is over on SoundCloud <a href="https://soundcloud.com/user-274016778/school-for-new-writers-episode-1-featuring-sandcroft" rel="nofollow">here</a> but the rest of the episodes are going to be on youtube for time reasons.</p>