---
Author: darklordcomp
Category: ''
Date: Sep 29th, 2016
Tags: []
Title: Podcast Resuming Tomorrow.
---

Hello my students! I apologize for not posting for a long time, real life decided to throw itself at me and I couldn't really tell her no. Now that she has been satisfied I'm going to be starting the School For New Writers Podcast up once again, Tomorrow at 5:30pm PDT is when we will begin recording so post your questions! I'm hoping for a shorter cleaner Podcast so please come down here and contribute! The first episode is over on SoundCloud here but the rest of the episodes are going to be on youtube for time reasons.