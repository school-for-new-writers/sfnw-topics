---
Author: ocalhoun
Category: ''
Date: Friday 5th of June 2015 @1:34am
Tags: []
Title: 'Field Trip: When is the best time to post a story? -- Answered with SCIENCE! '
---

<p>After a year of analysis and over 50,000 data points collected, it's finally time to let people in on my little secret project!</p><p>I've been analyzing the Fimfic feature box since about this time in 2014. I'd had enough of hearing rumors and hearsay about when the best time to post a story was. I decided to find out once and for all, with science!</p><p>Better still, it ended up producing pretty graphs, like this one:<br><img data-source="http://s7.postimg.org/gmcg0nvff/3_hourly.jpg" class="user_image" src="https://camo.fimfiction.net/VQ5n6DIGY7N32Io4kstsFw3PQCcY1vsI87U_mDqdob4?url=http%3A%2F%2Fs7.postimg.org%2Fgmcg0nvff%2F3_hourly.jpg" data-lightbox/=""></p><p>Because trying to post the whole thing in several different groups would be cumbersome, I've put the detailed analysis in a blog post:<br><a href="/blog/494106/when-is-the-best-time-to-post-a-story-answered-with-science" rel="nofollow">When is the best time to post a story? -- Answered with SCIENCE!</a></p><p>I hope you'll go there and perhaps learn something that will help you find success!</p>