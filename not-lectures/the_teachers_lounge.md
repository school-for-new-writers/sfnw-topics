---
Author: The Dean
Category: ''
Date: Sunday 24th of May 2015 @7:13am
Tags: []
Title: The Teacher's Lounge
---

<p>Hello, students!</p><p>We at the School For New Writers have been talking, and we've come up with a fantastic idea  to help you learn in a way that's a little more organic and a little less stuffy: The Teacher's Lounge! Our hard working professors will come together to talk about writing subjects that might be difficult (or simply less fun) to cover in lecture format. We'll then post a video of our discussion on YouTube, and a link to the video here in our very own forums for you to peruse to your heart's content!</p><p>We ask only one thing from you, and that's to post any ideas of subjects you think we should talk about here! If we see some good ideas we'll be sure to address them in our next video. But fear not, if there are no suitable suggestions then we will create our own subjects to speak of!</p><p>Go on now, class! Let's hear some suggestions!</p>