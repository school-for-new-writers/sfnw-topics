---
Author: darklordcomp
Category: ''
Date: Monday 9th of October 2017 @7:02am
Tags: []
Title: Right, I'm getting sick of posting this.
---

<p>Students, it is 12 in the morning for me, I don't want to be policing the threads right now. Stop posting threads without permission from professors. You are not allowed to and You've never been allowed to post them without permission. I'm going to bed.</p>