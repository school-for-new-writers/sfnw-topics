---
Author: The Dean
Category: ''
Date: Tuesday 5th of May 2015 @10:37am
Tags: []
Title: The School Directory Has Been Updated
---

<p>Hello, class! I've got some great news for you all today!</p><p>After hours of painstaking labor, multiple runs of a roughly eighty-song playlist, a terrible headache, searing eye pain, and a few labor law violations, the directory of the SFNW has finally been updated! Finally!</p><p>If you're new to the group, or are an older student who's been missing out on the lectures as they've been posted, then now is a great time to browse the <a href="/group/916/school-for-new-writers/thread/150166/meta-school-directory" rel="nofollow">School Directory</a> and catch up on our diverse library of lectures, all written by more experienced authors hoping to help the next generation get their start. The newly added lectures are as new as the last few days, and as old as 127 weeks! That's a lot of lectures, and the directory has practically doubled in size with this update. New lectures will also now be added to the directory as they're posted.</p><p>That'll be all, class. Now go fill those creative heads of yours with knowledge!</p>