---
Author: darklordcomp
Category: ''
Date: Saturday 30th of May 2015 @7:57am
Tags: []
Title: 'Follow up Exercise: Create a god.'
---

<p>Hello my adorable little students! Someone on my previous lecture on <a href="/group/916/school-for-new-writers/thread/181209/lecture-how-to-write-or-not-write-a-godgoddess#comment/4420754" rel="nofollow">How to not write a god/goddess</a> Said that a good way to get you guys used to making gods was to well have you make gods! So now I want you to try and create a believable character in this forum, use those big old brains of yours and get cracking! The minds the limit, it is suggested that you read the lecture first so that you get an understanding on what to avoid first before tackling this homework assignment.</p>