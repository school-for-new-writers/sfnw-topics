---
Author: Goldenwing
Category: ''
Date: Sunday 24th of May 2015 @7:10am
Tags: []
Title: 'Field Trip: Elements of Literature'
---

<p>Hey guys, hows it going. I recently came across this article on the elements of literature and, being the always-caring professor that I am, of course couldn't help but think of you all and your eager writings. As such, I thought I'd post a link here and share it with you lads.</p><p><a href="http://www.nps.gov/mora/learn/education/upload/background-elements-of-literature_sr.pdf" rel="nofollow">Here you go.</a></p><p>Share and Enjoy!</p>