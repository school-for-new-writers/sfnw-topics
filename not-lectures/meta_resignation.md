---
Author: SirTruffles
Category: ''
Date: Sunday 31st of May 2015 @1:51am
Tags: []
Title: 'Meta: Resignation'
---

<p>After an examination of my goals and priorities, I find myself left with less energy for this group than an admin or professor role requires. As such, I believe the only fair thing to do is call inactivity what it is and resign both positions. Please direct any admin-related business to the other mods.</p><p>Thank you to those who read and commented on my (regrettably few) lectures. I hope you found them helpful. Also a big thank you to the other mods who have been far more involved with getting this group back up and running than I. Apologies for not pitching in more myself.</p><p>Best of luck.<br>- SirTruffles</p>