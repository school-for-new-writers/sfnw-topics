---
Author: Goldenwing
Category: ''
Date: Tuesday 20th of January 2015 @5:03am
Tags: []
Title: 'General: Suggestion Box'
---

<p>Have a great idea for a lecture? Looking for information on a topic but unable to find anything in the archives? Scribble out an idea and place it in our suggestion box here! If a professor stops by, he may take you up on your suggestion and write a relevant lecture on the subject! Please refrain from holding conversations here; stay on topic!</p><p>In each post make sure to bold a possible lecture title, and explain why you think we need it, like in this example below:</p><blockquote><p><b>Lecture: Pink Party Pony</b><br>Write a lecture on how to write Pinkie Pie, because she's hard to write!</p></blockquote>