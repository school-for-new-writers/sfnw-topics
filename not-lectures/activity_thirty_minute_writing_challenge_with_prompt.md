---
Author: darklordcomp
Category: ''
Date: Friday 31st of January 2014 @1:09am
Tags: []
Title: 'Activity: Thirty Minute Writing Challenge with Prompt'
---

<p>Just as the title says, I want to see you all write a story in thirty minutes (Of your own time, not mine) This is meant too get your writing muscles moving and your brains thinking! </p><p>The goal is to write as much as possible within thirty minutes, the story does not need to be done by 30 minutes, after thirty minutes of writing check your word count and post a screen shot of how many you have on this forum, after that feel free to post the story on the site and link it here.</p><p>Prompt: Whats a little rivalry between sisters?</p>