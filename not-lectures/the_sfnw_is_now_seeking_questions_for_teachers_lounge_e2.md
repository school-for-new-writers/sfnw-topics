---
Author: Piquo Pie
Category: ''
Date: Thursday 20th of August 2015 @11:02am
Tags: []
Title: The SFNW is now seeking questions for Teacher's Lounge E2
---

<p>Good day folks,</p><p>Piquo Pie here with the announcement for the Teacher's Lounge Round 2. We are aiming to have a teachers lounge about once a month to answer questions that you have about writing. So, if you have a question about writing or a topic you want to know more about, and you want to hear your favorite professors bicker and debate while answering it, leave your questions below. </p><p>And in case you missed it, you can catch the first episode at the end of this post. Last time we answered the following questions or talked about the following topics.</p><p>How to make Characters More Realistic<br>In the idea saturated world we live in now, how can one be truly original anymore<br>How to show not tell a landscape<br>How do you write to make your story flow better<br>Realism vs artistic license<br>How do you write a better protagonist<br>How to integrate philosophy without directly discussing it.</p><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=fgqBH8ijAw4&feature=youtu.be" data-src="https://www.youtube.com/embed/fgqBH8ijAw4?autoplay=1" data-id="fgqBH8ijAw4" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/fgqBH8ijAw4/mqdefault.jpg"><div class="overlay"></div></div></div>