---
Author: Goldenwing
Category: ''
Date: Tuesday 20th of January 2015 @4:51am
Tags: []
Title: 'General: Study Hall'
---

<p>Come one, come all, to the extravagant school Study Hall! Sit under the watchful gaze of our faculty as you make a labor of love over your horse-based literature, and feel free to ask any questions that come to mind!</p><p>If you have a question specific to you, or which isn't answered in a lecture, then this is the place to post it! A professor will be along shortly to give you an individualized answer. Please ask just one question at a time, and don't ask another until your previous question is answered. Also, please head each post with a summary topic, and refrain from holding conversations. If you require further clarification about the answer you're given, send a PM to the relevant professor.</p><p>Here's an example question:</p><blockquote><p><b>Writing Pinkie Pie</b><br>I'm having some trouble with writing Pinkie Pie, my readers keep saying that I'm not doing it right and I have no idea why. Help!</p></blockquote>