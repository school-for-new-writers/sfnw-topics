---
Author: darklordcomp
Category: ''
Date: Tuesday 26th of July 2016 @6:32pm
Tags: []
Title: Classroom Compilation.
---

<p>To Save room for more lectures and other anouncements I am going to be unstickying all the classroom threads so that newer threads can be on the first page of the thread list more often. Any Classrooms that you can't find on the front page will now be listed in this permanent sticky thread.</p><p><a href="/group/916/school-for-new-writers/thread/223021/classroom-piercingsight" rel="nofollow">Classroom: PiercingSight</a></p><p><a href="/group/916/school-for-new-writers/thread/224037/classroom-goldenwing" rel="nofollow">Classroom: Goldenwing</a></p><p><a href="/group/916/school-for-new-writers/thread/241191/classroom-mourning-zephyr" rel="nofollow">Classroom: Mourning Zephyr </a></p><p><a href="/group/916/school-for-new-writers/thread/234288/classroom-bookplayer" rel="nofollow">Classroom: bookplayer</a></p><p><a href="/group/916/school-for-new-writers/thread/223313/classroom-bluegrass-brooke" rel="nofollow">Classroom: Bluegrass Brooke</a></p><p><a href="/group/916/school-for-new-writers/thread/266586/classroom-angius" rel="nofollow">Classroom: Angius </a></p>