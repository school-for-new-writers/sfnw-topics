---
Author: PegasusKlondike
Category: ''
Date: Tuesday 7th of January 2014 @5:19pm
Tags: []
Title: 'Field Trip: Not really informative, but true.'
---

<p>The title says it all. I was perusing one of my favorite websites when I stumbled upon this article by one of my favorite columnists. It's true enough pretty much all the way through.</p><p>Here you go: <a href="http://www.cracked.com/blog/4-weird-side-effects-learning-how-to-write/" rel="nofollow">4 Weird Side Effects of Learning How to Write</a></p>