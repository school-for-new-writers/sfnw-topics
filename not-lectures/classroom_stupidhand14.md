---
Author: stupidhand14
Category: ''
Date: Sunday 30th of July 2017 @9:50am
Tags: []
Title: 'Classroom: Stupidhand14'
---

<div class="bbcode-center" style="text-align:center"><p>The Classroom of Stupidhand14</p></div><p>Hello, and welcome to my classroom. For those of you who don't know (either because it's been too long since one was made, or you're new), classrooms are places for students to post questions for specific teachers. As the School's most recent professor, I felt it was finally time for me to set up my classroom.</p><p>Now, disclaimer: I am the least qualified professor. I have yet to truly write something great, nor do I have a degree in writing, nor over ten years in writing fanfiction (pony or otherwise). The only thing I can provide is time, so feel free to ask a question and I should get to it within 24 hours (no promises).</p><p>Reminder: Use the reply button in the top right of this post so I'm notified and can answer quickly.</p>