---
Author: darklordcomp
Category: ''
Date: Sunday 26th of June 2016 @11:05am
Tags: []
Title: 'Podcast/Leacture Episode 1: How to Write an OC and much more.'
---

<p>Hello my adorable students! It is I, your lord and savoir Darklordcomp posting this at four in the morning when I should be sleeping! Here is the first of many podcasts done by myself with guest <a href="/user/Sandcroft" rel="nofollow">Sandcroft</a>! I hope you all enjoy. </p><p><a href="https://soundcloud.com/user-274016778/school-for-new-writers-episode-1-featuring-sandcroft" rel="nofollow">School for New Writers Podcast</a></p><p>In the Podcast we cover: <br>How to write Mystery, Slice of Life, and How to create a believable OC.</p>