---
Author: The Dean
Category: ''
Date: Tuesday 15th of May 2018 @12:07am
Tags: []
Title: '!!PODCAST!! - School For New Writers Stream'
---

<div class="bbcode-center" style="text-align:center"><p><span style="font-weight:bold;font-size:2em">We're doing a School For New Writers Podcast!</span></p></div><div class="bbcode-center" style="text-align:center"><p>We give out tips and answer questions about writing. Today we are covering different writing Tropes, Pacing and much more! </p><p>Feel free to ask questions in the stream chat or <a href="https://discord.gg/MhuYbYU" rel="nofollow">join our discord</a> and asks questions there!</p></div><div class="bbcode-center" style="text-align:center"><p><a href="https://www.youtube.com/watch?v=j7ZrtW3_KT8" rel="nofollow"><span style="font-size:2em">COME CHECK IT OUT!</span></a></p></div><div class="bbcode-center" style="text-align:center"><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=j7ZrtW3_KT8" data-src="https://www.youtube.com/embed/j7ZrtW3_KT8?autoplay=1" data-id="j7ZrtW3_KT8" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/j7ZrtW3_KT8/mqdefault.jpg"><div class="overlay"></div></div></div></div><div class="bbcode-center" style="text-align:center"><p><i>Starting at 8:30-ish EST</i></p></div><p>- The Dean</p>