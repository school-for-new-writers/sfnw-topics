---
Author: Goldenwing
Category: ''
Date: Friday 15th of May 2015 @12:15pm
Tags: []
Title: 'REMINDER: PLEASE READ THE RULES'
---

<p><a href="/group/916/school-for-new-writers/thread/196570/meta-rules-and-guidelines" rel="nofollow">Right here. Click on this and read it all, top to bottom, bottom to top.</a></p><p>If  you get nothing else from that thread, just remember that <i>students are not allowed to start threads without permission</i>. Any illegal threads will be locked and deleted within 24 hours. The School For New Writer's is not a place for you to come and ask for editors or proofreaders, or to advertise your story; there are other groups for these purposes. If you have any questions which can't be answered in one of <a href="/group/916/school-for-new-writers/thread/150166/meta-school-directory" rel="nofollow">our many lectures</a>, then please ask them in the<a href="/group/916/school-for-new-writers/thread/149776/general-study-hall" rel="nofollow"> Study Hall</a>, where a professor will answer it as soon as they can come up with a good answer.</p><p>Thank you.</p>