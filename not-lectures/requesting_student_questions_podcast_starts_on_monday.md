---
Author: darklordcomp
Category: ''
Date: Friday 11th of May 2018 @7:01pm
Tags: []
Title: Requesting Student Questions! Podcast Starts on Monday
---

<p>So Starting on Monday I will be restarting the School For New Writers Podcast. As you all know I wanted to restart it early, but college and other things go in the way and took up most of my time. Now that I am free, I intend to make this a weekly or biweekly occurrence. There are going to be some format changes to go with the new podcast though. </p><p>The Podcast will be live streamed from Youtube, Monday at 5:30 PM PDT or 8:30 PM EST. The Live stream will be about two and a half hours long and will be set up as so. </p><p>Hour 1: Answering Student Questions asked specifically for the Podcast.<br>Hour 2: Talking about topics the guests want to talk about, such as what fanfics we are reading, what tips we want to give, tropes, etc. <br>Thirty Minutes: Stream Chat Q and A, if you have a question, ask it on the live stream chat and we'll answer it.</p><p>We Hope to see you there! Post any questions here and on Monday a new thread will be posted so that you all can get links to the live stream.</p><p>-Darklordcomp</p>