import sys


def wl(string: str):
    sys.stdout.write(string + '\n')


def wl_flush(success: bool = True):
    sys.stdout.flush()
    if success:
        sys.exit(0)
    else:
        sys.exit(1)
