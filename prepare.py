import json
import os
import codecs
import re

import date_converter

from util import wl, wl_flush

path = 'lectures/'

fileList = os.listdir(path)
for file in fileList:

    f = codecs.open(os.path.join(path + file), 'r', encoding='utf-8-sig')
    j = f.read()

    if j is not '':
        data = json.loads(j, encoding='utf-8-sig')
        data['Tags'] = []
        data['Category'] = ''
        data['Date'] = re.sub(r"(\d)(st|nd|rd|th)", r"\1", data['Date'])
        data['Date'] = date_converter.string_to_string(data['Date'].upper(), '%A %d OF %B %Y @%I:%M%p', '%a, %d %b %Y %H:%M:%S')

        with codecs.open(os.path.join(path + file), 'w', encoding='utf-8-sig') as fh:
            json.dump(data, fh, ensure_ascii=False, indent=4, sort_keys=True)

        print(f"Parsed: {data['Title']}")

    f.close()


for index, file in enumerate(fileList):
    os.rename(os.path.join(path, file), os.path.join(path, file
                                                     .replace('[', '')
                                                     .replace(']', '')
                                                     .replace('__', '_')
                                                     # Lowercase
                                                     .lower()
                                                     # Lecture amalgams
                                                     .replace('lecturefaux', 'lecture_faux')
                                                     .replace('lecturefield', 'lecture_field')
                                                     # Protip amalgams
                                                     .replace('tipfaux', 'tip_faux')
                                                     .replace('tipfield', 'tip_field')
                                                     .replace('pro_tip', 'protip')
                                                     # Guide amalgams
                                                     .replace('guidefield', 'guide_field')
                                                     # Sublecture amalgams
                                                     .replace('sub_lecture', 'sublecture')
                                                     ))
    wl(f"File {file} has been renamed.")

wl_flush()
