import codecs
import json
import os

import frontmatter
from frontmatter import Post


def transform(src_dir: str, dest_dir: str):
    out_files = []
    files = os.listdir(src_dir)

    os.makedirs(dest_dir, exist_ok=True)

    for file in files:
        if '.json' not in file:
            continue

        f = codecs.open(os.path.join(src_dir + file), 'r', encoding='utf-8-sig')
        j = f.read().strip()
        if j is not '':
            data = json.loads(j, encoding='utf-8-sig')

            post: Post = Post(data['Body'].strip())
            post['Title'] = data['Title']
            post['Author'] = data['Author']
            post['Date'] = data['Date']
            post['Category'] = data['Category']
            post['Tags'] = data['Tags']

            out_files.append(codecs.open(os.path.join(dest_dir, file.replace('.json', '.md').lower()), 'w', encoding='utf-8').write(frontmatter.dumps(post)))


transform('lectures/', 'lectures/')
transform('not-lectures/', 'not-lectures/')
