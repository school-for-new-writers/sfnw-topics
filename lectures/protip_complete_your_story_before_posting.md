---
Author: darklordcomp
Category: ''
Date: 'Thu, 24 Sep 2015 22:53:00 '
Tags: []
Title: '[Pro Tip] Complete your story before posting.'
---

<p>Professor Darklordcomp's first pro tip! </p><p>This is something I need to work on myself as a writer, but it is a good habit to get into when creating a story. Before submitting your tale onto this site try and have it completed are as close to completed as possible, this allows you to submit a chapter on your own time and keeps the passion for your story white hot within a reader since they don't have to wait very long for a new chapter to be released.</p>