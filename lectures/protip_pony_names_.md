---
Author: bookplayer
Category: ''
Date: 'Wed, 16 Sep 2015 04:05:00 '
Tags: []
Title: '[Pro Tip] Pony Names '
---

<p>I wanted to respond to that other thread, then I remembered "Hey, I'm a professor and I can post official threads!"</p><p>Someone just asked for advice on pony naming, and it turns out I have a tip: <b>I use paint colors.</b> The paint chips you find at hardware stores, or googling "paint colors" will bring up a bunch of brands, and they all name their colors things like "Rosedust" or "Leather Bound" or "Sky High" that make perfectly good pony names. You have to click through them, some of the more color-related ones don't work as well, but you can usually find a good list of names to pick from if you have a bit of time and look through a few companies.</p><p>Anyway, people can use this thread for other suggestions, too. This is just my trick. <img alt=":twilightsmile:" class="emoticon" src="https://static.fimfiction.net/images/emoticons/twilightsmile.png"></p>