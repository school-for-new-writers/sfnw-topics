---
Author: PegasusKlondike
Category: ''
Date: 'Mon, 19 Aug 2013 13:50:00 '
Tags: []
Title: 'Lecture/Field Trip: Grammar with a Wrestler'
---

<p>DemonOJM recently sent me a message, and it was the most awesome thing I have ever seen in my life as a writer. They're called CM Punk's Grammar Slams, and they are funny and informative ways of learning the subtle differences between homonyms and their proper usage. Check them out on Youtube. Enjoy <img alt=":pinkiesmile:" class="emoticon" src="https://static.fimfiction.net/images/emoticons/pinkiesmile.png"></p><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=VFEEa-jibhs" data-src="https://www.youtube.com/embed/VFEEa-jibhs?autoplay=1" data-id="VFEEa-jibhs" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/VFEEa-jibhs/mqdefault.jpg"><div class="overlay"></div></div></div>