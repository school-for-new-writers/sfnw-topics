---
Author: Piquo Pie
Category: ''
Date: 'Sun, 01 Mar 2015 10:52:00 '
Tags: []
Title: 'Pro-Tip: How to Vary Sentence Length and Why It''s Important!'
---

<p><img data-source="http://media-cache-ec0.pinimg.com/736x/59/41/2a/59412a0d6fe98e97890e2acc761914e7.jpg" class="user_image" src="https://camo.fimfiction.net/7USPh_ULOFFAwUfsnFD0AMlDUhRVjDMO8fe0SbsJrIo?url=http%3A%2F%2Fmedia-cache-ec0.pinimg.com%2F736x%2F59%2F41%2F2a%2F59412a0d6fe98e97890e2acc761914e7.jpg" data-lightbox/=""></p><p>Yes, I cheated with this lecture. But if you think you could explain it more eloquently than that then be my guest and use the space below.</p>