---
Author: TheBandBrony
Category: ''
Date: 'Mon, 02 Jul 2012 22:20:00 '
Tags: []
Title: 'Pro-Tip: Shipping!'
---

<p><span style="color:#C0C0C0">Oh noes a shipping thread. All hope is lost.</span></p><p>Hello there, I'm TheBandBrony. I've had a lot of discussions on the topic of shipping with new authors over the past few days, and every single time the conversation has turned to ponyXpony, I've had to constantly make one fact abundantly clear.</p><p><span style="font-size:1.75em">If you don't like the ponies you ship, your story will be shit.</span></p><p>Seriously, is it really even necessary to say that? If you don't like RariJack, here's an idea! <b>Don't write RariJack.</b> Of course it helps if you don't suck at writing, either, but you can be the most talented author in the world and this rule will still kill you every time. You don't have to love every pairing you write about, but if you can't stand the thought of Twilight Sparkle and Applejack taking a roll in the hay, you're probably best off not writing about it.</p>