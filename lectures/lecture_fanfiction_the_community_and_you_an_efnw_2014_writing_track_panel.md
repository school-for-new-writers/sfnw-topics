---
Author: Piquo Pie
Category: ''
Date: 'Fri, 18 Jul 2014 04:30:00 '
Tags: []
Title: 'Lecture: Fanfiction the Community and You, an EFNW 2014 writing track panel'
---

<p>Good day,</p><p>Participating in fanfiction isn’t like being a normal reader or writer; it’s a much more level playing field, with plenty of opportunities for dialogue between author and audience. In the panel embedded below we cover how to reach out and interact in the fanfiction community, from getting help forging relationships, to working with or being an editor, to collaborating on projects.</p><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=Bvdgni7FeUg" data-src="https://www.youtube.com/embed/Bvdgni7FeUg?autoplay=1" data-id="Bvdgni7FeUg" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/Bvdgni7FeUg/mqdefault.jpg"><div class="overlay"></div></div></div>