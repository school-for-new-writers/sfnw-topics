---
Author: Jewbacca
Category: ''
Date: 'Tue, 11 Jun 2013 15:42:00 '
Tags: []
Title: 'Sub-Lecture:  Interrobangs and When and How to Use Them'
---

<p>You may not know the term, but you definitely know what they are.</p><p>Interrobangs, or interbangs, are a combination of a question mark and exclamation point generally to show questions that are asked in shock or anger. They can often be used well in a sentence because they can draw out a lot of emotion when asking a question or in an interrogation.</p><p>E.G.</p><blockquote><p>Where the hell did my last piece of cake go<b>?!</b></p></blockquote><p>They're used a lot to add tone to a character that is in a state of surprise, anger, and other exclamatory emotions, but used in the form of a question. They should <i>only</i> be used as a question if the character is genuinely exclamatory.</p><p>Using multiple interrobangs is unnecessary, and makes you look like a dumbass.</p><p>i.e.</p><blockquote><p>Why are you doing this to me?!?!?!?! WHY?!?!?!?!</p></blockquote><p>Doesn't that just look dumb? You only have to use them as "?!" or "!?," and only use one at a time in a sentence. I hope this little speech helps you the next time you inject them into your writing, have a good rest of your day.</p>