---
Author: TheBandBrony
Category: ''
Date: 'Thu, 01 Nov 2012 16:57:00 '
Tags: []
Title: 'Pro-Tip: insta-shipping (and why I hate you for writing it)'
---

<p>Romantic bonds take time to form, regardless of whether you're in the real world or Gayass-Ponyfanficlandistan. If you can form an  honest, deep bond between two characters in under 5k words, you're probably doing it wrong.</p><p>This doesn't mean your shipfics have to be a million words long. Just, please, put a little more thought into <i>why</i> your characters want to make out behind a dumpster in the back lot of Denny's, not just <i>how</i> they go about doing it.</p>