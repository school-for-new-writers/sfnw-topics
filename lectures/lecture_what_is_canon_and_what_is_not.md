---
Author: Lore-Lei
Category: ''
Date: 'Mon, 06 Apr 2015 11:51:00 '
Tags: []
Title: 'Lecture: What is canon and what is not'
---

<p>Hello class! Mel here.</p><p>Today's lecture is going to be about making difference between what is canon, and what is not. As we all know, canon is considered any material that is official as a part of a story's (or in this case, show's) universe. This can be interpreted in many ways however.</p><p>One of the most common things I see is the <i>"Everything that is in the show is canon."</i> way of thinking that is quite counterproductive. People who go by that thinking are usually the first ones who end up coming up with mad overanalyzations and <a href="https://www.youtube.com/watch?v=-Sym1h72K2c" rel="nofollow">dumb theories about Pinkie Pie being a changeling.</a></p><p><b>No. 1</b>: Remember, the show is not made by only one person, and one episode takes a whole lot more time to create, than write. This is a gigantic factor in the whole ordeal since there are years between some of the episodes, as such minor details that are easy to skip over might be not making sense if not outright contradicting each other. Add the fact that said two episodes might have been written by a completely different person and you'll see how extremely flexible canon is. </p><p><b>No. 2:</b> Note that the series is not completely planned out from beginning to the end, and sometimes old canon must be sacrificed for a new, better one. These sacrifices might include as small things as Granny Smith's age to as colossal as Princess Luna as a whole. I'll be getting to that later.</p><p><b>No. 3</b>: Breaking canon for subliminal messages, patching loopholes, or simply for comedic effects. Remember this scene?<br><img data-source="http://awesome.redial.net/forscience.png" class="user_image" src="https://camo.fimfiction.net/gDJdFBFmC9tbmcQ6akDHnItqoDFykc76G8OqLNOnexQ?url=http%3A%2F%2Fawesome.redial.net%2Fforscience.png" data-lightbox/=""><br>The point of this whole scene was to show that Twilight tries her absolute hardest to understand how Pinkie Pie works, monitoring every, single, carbon fiber of the pony. Exactly for this reason, the scene has been exaggarated to the point it makes no sense neither in world-building nor character development, and as such, breaking canon. Looks good? Sure. Makes sense? Not really.</p><hr><p>The other type of canon I'd like to talk about might be an expansion to the first type, that runs by the name <i>'Word of God'</i>. This pretty much says that <i>"Everything the creator of the content says is canon."</i>.<br>This is, once again, counterproductive since there are no creator, but creatorS. Each and every writer of the members working on the show interprets certain things slightly differently, each having their certain strenght and weaknesses. Not only that, but even these 'gods' are influenced by other 'gods' of higher rank, namely the guys at Hasbro.</p><p>As, and I mentioned above, the series is not completely planned out from beginning to end. This means that sometimes the writers have no idea how to explain certain things in the universe, since the reason was of something outside of said universe. </p><p>Remember what I said about Princess Luna?<br><img data-source="http://3.bp.blogspot.com/-CHaFRH4BS30/TqCbb-93WyI/AAAAAAAAO_A/yDRCZ2ZgzgA/s1600/Capture.JPG" class="user_image" src="https://camo.fimfiction.net/YymAnZxKAISMAL72HP_4V2Kmj77yrdNTTL-19_tB1Pk?url=http%3A%2F%2F3.bp.blogspot.com%2F-CHaFRH4BS30%2FTqCbb-93WyI%2FAAAAAAAAO_A%2FyDRCZ2ZgzgA%2Fs1600%2FCapture.JPG" data-lightbox/=""><br>Note the <i>'I just made that up'</i> part. The real reason behind Luna's change was that old Luna was designed to be adorable and make it easier for the audience to forget. Luna Eclipsed, however could not work with that Luna, not only because she looked absolutely nothing like Nightmare Moon, but because she could not be frightening enough to be believable. As such, Luna's design was swapped and the writers pretended like nothing happened. Thus, the new canon became that Princess Luna never even had an S1 design before.</p><hr><p>See now? This is how you differentiate canon and not-canon things. You need not to search in the universe, but outside of it, and think like a writer.</p><p>Here are a couple questions you can ask yourself in case you are not sure in the process:<br><b>1. Does this make sense in terms of world-building and character depth?</b> <span style="font-size:0.75em">(Why would anyone build a NASA lab in a library? How and where did Twilight learn to use these things?)</span><br>If 'No' or the question spawns way too much inconsistent other questions, it's most likely not canon.<br><b>2. Are there any external reasons for this to happen?</b> <span style="font-size:0.75em">(Did Chrysalis blow her cover because of not enough screentime? Did Pinkie know about X because she was used as a joker card?)</span><br>If 'Yes', feel free to disragard it as not canon.<br><b>3. Are the writers exaggerating things for humour?</b><br>Mostly Pinkie Pie jokes or cartoon phsyics apply here. If 'Yes', feel free to disregard it.<br><b>4. Are the writers' answers or explanations sound like an excuse? Or something they didn't think about but try to make up something for it because of the question?</b><br>If 'Yes' you can most likely disregard it and go with your own interpretations.</p><p>Canon is, unlike others might think, not a rock solid, unbreakable and saint thing. However, that doesn't mean you can disregard well estabilished and basic things without the AU tag. Your goal is to find the point of balance between taking too much or too little things as your canon.</p><hr><p>One last thing: I've seen people wearing horse blinders throwing fits over certain character's behavior from time to time, most notably that outlash against Pinkie Pie when she behaved like an idiot with Fluttershy in Filli Vanilli. Don't be one of those people and take every tid-bit of act the characters make into account. Know that the staff is very much is into sacrificing characters on the Altar of Cheap Laughs instead of making proper jokes. <span class="spoiler">Especially Amy 'Comedy Gold' Rogers.</span> Examples of character sacrifices are including, but not limited to:<br><img data-source="http://s2.dmcdn.net/CuFBm/526x297-guv.jpg" class="user_image" src="https://camo.fimfiction.net/RF22cZ7jJvHgZc014UNJKg8enDIfAcdynjz0kSydWIU?url=http%3A%2F%2Fs2.dmcdn.net%2FCuFBm%2F526x297-guv.jpg" data-lightbox/=""><br><img data-source="https://mlpforums.com/uploads/monthly_03_2015/blogentry-23942-0-26864100-1425659790.png" class="user_image" src="https://camo.fimfiction.net/pdLig_u9GoZGhqet5Ba-ROSClJpgysvErU7_RjozdzY?url=https%3A%2F%2Fmlpforums.com%2Fuploads%2Fmonthly_03_2015%2Fblogentry-23942-0-26864100-1425659790.png" data-lightbox/=""><br><img data-source="https://derpicdn.net/img/view/2012/7/3/30045__safe_twilight+sparkle_rarity_applejack_screencap_caption_look+before+you+sleep_hatless.jpg" class="user_image" src="https://derpicdn.net/img/view/2012/7/3/30045__safe_twilight+sparkle_rarity_applejack_screencap_caption_look+before+you+sleep_hatless.jpg" data-lightbox/=""></p><p>I've been Mel Syreth. See you on the next class.</p>