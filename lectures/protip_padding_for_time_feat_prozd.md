---
Author: PiercingSight
Category: ''
Date: 'Sun, 06 Aug 2017 06:33:00 '
Tags: []
Title: 'Pro Tip: Padding for.... time? - Feat. ProZD'
---

<p>In animation, sometimes they add gags or awkward moments that completely break the pacing in order to fill the time of the episode. Other times, they cut too much.</p><p>The same happens to a lot of beginning writers as well, but I can't conceive of any reason why. There is no time to fill, no word number requirement, or even word constrain, and yet, many authors feel the need to add filler.</p><p>Maybe it's because they feel the pacing is too fast? The story is too short? The romance is too quick? And they are either too lazy to figure out how to expand it, or simply just don't know how?</p><p>I'm not entirely sure why, but please don't add pointless stuff to your fic because you think it needs to have a certain length/pacing to it. Only add things that actually contribute something to the story. I may write a bigger lecture on this topic later.</p><p>For now, have a hilarious example of what not to do:</p><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=cKe4DhsIcsw" data-src="https://www.youtube.com/embed/cKe4DhsIcsw?autoplay=1" data-id="cKe4DhsIcsw" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/cKe4DhsIcsw/mqdefault.jpg"><div class="overlay"></div></div></div><p>The Best Words,<br>PiercingSight</p>