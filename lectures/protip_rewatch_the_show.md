---
Author: XiF
Category: ''
Date: 'Sat, 14 Sep 2013 16:29:00 '
Tags: []
Title: 'Protip: (Re)Watch the Show'
---

<p>Fluttershy may be a miserable pony, and maybe nopony would notice if she disappeared, but she does not talk like this:</p><blockquote><p><span style="font-size:0.5em">Um hi um Twilight....</span> <span style="font-size:0.5em">um... how um are um you.... </span><span style="font-size:0.5em">um doing um to um day... um?</span></p></blockquote><p>"20% cooler" and "10 seconds flat" are not the only two things Rainbow Dash have ever said. In fact, she's only said those things once; I'm pretty sure.</p><p>When you're writing, don't be afraid to brush up on canon by watching an episode or two (or all of them; I don't know) of the show (<i>several times</i>). It can really help you put yourself in the character's shoes (or horseshoes, whatever). Pay attention to setting. Pay attention to what the characters say and do, <i>and why</i>. If you're writing a story about Rarity, rewatch "Sweet and Elite"; if you're writing Princess Chrysalis' <s>earth</s>equestria-shattering back-story in which it turns out she's secretly a changeling, why not watch the season 2 finale? If you're describing the Everfree Forest in your 100-word magnum opus, why not pull up a reference image? You may think you know everything there is to know about everything My Little Pony, (way to go, you nerd) but the chances you haven't analyzed it like you would some inane novella in English class. </p><p>So, my point, I think, is to watch the show.</p>