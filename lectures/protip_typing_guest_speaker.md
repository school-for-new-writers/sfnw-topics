---
Author: zaczac111
Category: ''
Date: 'Fri, 29 Jun 2012 23:10:00 '
Tags: []
Title: 'Pro-Tip: Typing "Guest Speaker"'
---

<p>Now, first let me say I hope I'm not over-stepping my boundaries by posting this..</p><p>Anyways, onward to the actually tip. My tip to writing is simple; practice typing with good grammar in other places than just stories. What I mean by this is to take your time and correct yourself while making comments on any other site you visit. With 'internet' speak such a big part of the world now, trimming down words into acronyms like 'Lol', 'Lmao', and the such means less practice in the long run. Also, misspelling on purpose for long words to simply shorten them down to make it easier to type faster can potentially slow down your writing if you need to use these words because you'll need to take the time to remember how to spell them out.</p><p>So, instead of going around the web and using shortcuts for words, take a little more time to type out each word. This has helped me immensely, so I hope that it'll be able to help at least one other as well. Happy writing!</p>