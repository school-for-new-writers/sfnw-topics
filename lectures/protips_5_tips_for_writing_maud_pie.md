---
Author: Lore-Lei
Category: ''
Date: 'Sat, 02 May 2015 15:18:00 '
Tags: []
Title: 'Pro-tips: 5 tips for writing Maud Pie'
---

<p>You want to write about Maud? Let me help. People can't write her properly most of the time. Here are a couple tips.</p><p><b>1.</b> Maud doesn't talk very much<br>Surprise. Don't make her speak up too often. Maud is a listener. Make her use body language when you can to make up for it.</p><p><b>2.</b> Maud doesn't talk for very long.<br>She uses simple sentences. Short ones. Like I do. Beige prose.</p><p><b>3.</b> Maud is not Twilight<br>Don't use complicated words. Overly extensive sentences and scientific sounding wording like this make a false portrayal of Maud.</p><p><b>4.</b> ...<br>Maud is slow. She takes a second or a half to react. Emphasise this by making her blink or stare for a little bit. Maud also takes her time. Make sure actions are true to that.</p><p><b>5.</b> Maud is literal<br>You don't need to make rock jokes out of everything. Even she wouldn't get it. Maud takes things literally. Symbosism and methaphors bounce off her.</p><p><b>BONUS.</b> Maud can show emotions.<br><img data-source="https://derpicdn.net/img/2014/3/21/581184/large.png" class="user_image" src="https://derpicdn.net/img/2014/3/21/581184/large.png" data-lightbox/=""><br>Fear, sadness, even happiness and anger. Feel free to make her show subtle signs of these. Especially when one'd recieve huge doses of them. <br>You can also use this to unnerve the audience by the way.</p>