---
Author: Piquo Pie
Category: ''
Date: 'Wed, 26 Feb 2014 11:14:00 '
Tags: []
Title: 'Guide/Field Trip: Everything You Didn''t Even Know You Wanted to Know about
  Writing Mechanics.'
---

<p>Good day students. Normally I wouldn't post entirely video oriented lesson within the school forums, but alas <a href="/user/GaryOak" rel="nofollow">Gary Oak</a> has done a wonderful job on his <a href="http://www.youtube.com/watch?v=Of0d9x-BKe4" rel="nofollow">video on writing mechanics</a> and why they are important to writing a great story. He covers everything from what publishers look at to the level of professionalism that is acceptable based on your target audience, and the reason I am including this here.</p><p>Gary Oak does a live edit on screen looking at Past Sins's prelude and first chapter. Not only does he explain good and bad mechanics, but he also explains why certain things are considered good and bad.</p><p>I hope you enjoy, and more importantly learn something.</p>