---
Author: Piquo Pie
Category: ''
Date: 'Mon, 02 Jun 2014 16:57:00 '
Tags: []
Title: 'Lecture: Panel on Plot with Pen Stroke, GarOak, BronyWriter and Piquo Pie'
---

<p>Good day,</p><p>I headed up a online panel talking about Plot for Everfree Northwest. We had some really good conversation on plot from topics like foreshadowing, to sub-plots, lead-ins, mood setting, and more. I hope you enjoy it. </p><div class="embed-container bbcode-embed" data-original-src="https://www.youtube.com/watch?v=6YyUPf4wLNw" data-src="https://www.youtube.com/embed/6YyUPf4wLNw?autoplay=1" data-id="6YyUPf4wLNw" data-origin="YouTube"><div class="video"></div><div class="placeholder" data-origin="YouTube"><img src="https://img.youtube.com/vi/6YyUPf4wLNw/mqdefault.jpg"><div class="overlay"></div></div></div>