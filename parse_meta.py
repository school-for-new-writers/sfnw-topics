import json
import os
import codecs
from typing import List

path = 'lectures/'
tags: List[str] = []
categories: List[str] = []
authors: List[str] = []

fileList = os.listdir(path)
for file in fileList:

    f = codecs.open(os.path.join(path + file), 'r', encoding='utf-8-sig')
    j = f.read()

    if j is not '':
        data = json.loads(j, encoding='utf-8-sig')

        j_cat: str = data['Category']
        j_auth: str = data['Author']
        j_tags: List[str] = data['Tags']

        if j_cat not in categories:
            categories.append(j_cat)

        if j_auth not in authors:
            authors.append(j_auth)

        for t in j_tags:
            if t not in tags:
                tags.append(t)

        print(f"Parsed: {data['Title']}")


with codecs.open('_tags.json', 'w', encoding='utf-8-sig') as ft:
    json.dump(tags, ft, ensure_ascii=False, indent=4, sort_keys=True)

with codecs.open('_categories.json', 'w', encoding='utf-8-sig') as fc:
    json.dump(categories, fc, ensure_ascii=False, indent=4, sort_keys=True)

with codecs.open('_authors.json', 'w', encoding='utf-8-sig') as fa:
    json.dump(authors, fa, ensure_ascii=False, indent=4, sort_keys=True)
