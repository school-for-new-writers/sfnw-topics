# This repo is hosted under dual-license

## Code

License for the code part can be found in [LICENSE-CODE.md](LICENSE-CODE.md) and 
licenses the following:

* `/`
* `/public/assets/`
* `/templates/`

## Lectures

License for the lectures part can be found in [LICENSE-LECTURES.md](LICENSE-LECTURES.md)
and licenses the following:

* `/lectures/`
* `/not-lectures/`