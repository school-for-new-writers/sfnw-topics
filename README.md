# School for New Writers lectures repository

This repository contains all threads that can be found on the School for
New Writers group's forum on FiMFiction.

They will be cleaned up, tagged, and categorized. All non-lecture topics
will be removed or moved.

The entire database in its current state can be viewed [here](https://sfnw.netlify.app/).


## Contributing

### Building

To build `.html` files for local browsing, run `build.py` script.
Generated files will appear in `/public` directory.

### Reading data

* **_authors.json** contains a list of all authors parsed from the `Author`
field of each lecture

* **_tags.json** contains a list of all tags parsed from the `Tags`
field of each lecture

* **_categories.json** contains a list of all categories parsed from the
`Category` field of each lecture

### Data updating guidelines

Before modifying data it is best to pull up-to-date data from the master
branch of this repo.

#### Handling this repository

All modifications should be done and pushed to a named branch, created
via, for example, `git branch john-doe`.

Before pushing data back to the remote repo, run `parse_meta.py` to start
a Python script to update the list of authors, categories, and tags.

#### Tagging and categorization

Before adding or modifying tags or categories check what the existing
ones are and see whether you can use any of the existing ones. It is
to ensure that lectures are not split between similar categories or tags,
for example some under `tools` and some under `tooling`.

#### Changing authors

Author should be changed only if it's a guest lecture written by someone
else and posted by a member of the staff.

#### Moving

If a lecture isn't, in fact, a lecture, but a pro-tip, prompt, classroom,
meta, or anything in that vein – it should be moved to 
[`non-lectures`](/non-lectures) directory.
